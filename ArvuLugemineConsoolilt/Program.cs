﻿using System;
using System.Collections.Generic;

namespace ArvuLugemineConsoolilt
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            
            int arv; // = ??? // kuidas saada vastus (string) arvuks
            string vastus;
            // mõned võimlaused - 1. Parse

            Console.WriteLine("\nEsimene variant: Parse");
            Console.Write("Anna üks arv: ");
            vastus = Console.ReadLine();

            arv = int.Parse(vastus); // see on tehe, millega saaks teksti teisendada arvuks
            Console.WriteLine($"arv on {arv} ja tema ruut on {arv*arv}");
            
            Console.WriteLine("\nTeine variant: TryParse");


            // kui nüüd vastus ei ole arvu moodi, siis saame veateate

            // teine võimalus  2. TryParse (vaata siin umbes süntaksit, homme saame selgema pildi)
            Console.Write("Anna üks arv: ");
            vastus = Console.ReadLine();


            if (int.TryParse(vastus, out arv))
            {
                Console.WriteLine($"arv on {arv} ja tema ruut on {arv * arv}");
            }
            else
            {
                Console.WriteLine($"see pole miski arv: {vastus}");
            }

            Console.WriteLine("\nKolmas variant: TryParse tsükliga");

            // sellise küsimise võiks panna ka tsüklisse

            Console.Write("Anna üks av: ");
            while( !int.TryParse(Console.ReadLine(), out arv ))
            {
                Console.Write("see pole arv, anna üks korralik arv: ");
            }
            Console.WriteLine($"arv on {arv} ja tema ruut on {arv * arv}");

            Dictionary<string, int> vanused = new Dictionary<string, int>();
            string nimi = "*";
            int vanus;
            while(nimi != "")
            {
                Console.Write("Anna nimi (lõpetamiseks Enter): ");
                nimi = Console.ReadLine();
                if (nimi != "")
                {
                    Console.Write($"anna {nimi} vanus: ");
                    while (!int.TryParse(Console.ReadLine(), out vanus)
                        || vanus < 18  // kontrollime, et ei oleks liiga noor  
                        || vanus > 100 // kontrollime, et ei oleks liiga vana 
                        )
                    {
                        if (vanus != 0) Console.WriteLine($"{vanus} liiga noor või vana");
                        Console.Write($"viga juhtus\nanna {nimi} vanus: ");
                    }
                    vanused[nimi] = vanus;
                }
            }
            // kontrolliks trükime välja
            foreach(string x in vanused.Keys)
                Console.WriteLine($"{x} vanusega {vanused[x]}");

            // nüüd oli vaja leida noorim meite seast
            // kuidas seda küll teha

        }
    }
}
