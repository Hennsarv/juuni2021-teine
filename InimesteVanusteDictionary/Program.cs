﻿using System;
using System.Collections.Generic;

namespace InimesteVanusteDictionary
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            // sul on vaja kuskil pidada inimeste ja nende vanuste nimekirja
            // mis võimalused sul on
            // list - nimesid ainult, see ei sobi
            // dictionary - nimesid,vanused
            // Key - nimi
            // Value - vanus
            Dictionary<string, int> inimesed = new Dictionary<string, int>()
            {
                {"Henn", 66 },
                {"Ants", 40 },
                {"Peeter", 28 },
            };
            foreach (string nimi in inimesed.Keys) Console.WriteLine(nimi); // ainult nimed (Keys)
            foreach (int vanus in inimesed.Values) Console.WriteLine(vanus); // ainult vanused (Values)
            foreach (var x in inimesed) Console.WriteLine($"{x.Key} vanusega {x.Value}"); // paarid
            // var sõnast räägime homme
            foreach (string nimi in inimesed.Keys) Console.WriteLine($"{nimi} vanusega {inimesed[nimi]}");
            // iga key ja selle vastava value


            // teeme prooviks teise dictionary
            // meil on nimekiri Maakondadest - nr, maakonna nimi
            Dictionary<int, string> maakonnad = new Dictionary<int, string>();

            // meil oleks vaja sünnipäevade nimekirja
            // nimi, sünnipäev
            Dictionary<string, DateTime> sünnipäevad = new Dictionary<string, DateTime>
            {
                {"Henn", new DateTime(1955,03,07) },
                {"Ants", new DateTime(1971,07,20) },
                {"Peeter", new DateTime(1993,1,13) },
            };

            // loeme mõne sünnipäeva 
            Console.Write("anna nimi: ");
            string uusnimi = Console.ReadLine();
            Console.Write("Anna tema sünnipäev");
            DateTime sünnipäev = DateTime.Parse(Console.ReadLine());
            sünnipäevad.Add(uusnimi, sünnipäev);

            foreach (var x in sünnipäevad) Console.WriteLine( $"{x.Key} sünnipäev on {x.Value.ToShortDateString()}");

            // kes on kõige noorem
            // krt seda teab

            int oletus = 1000;
            string kes = "";
            double summa = 0;
            
            foreach (var x in inimesed)
                if (x.Value < oletus)
                //(kes, oletus, summa) = (x.Key, x.Value, summa + x.Value);
                {
                    kes = x.Key;
                    summa += (oletus = x.Value);
                }
            
            Console.WriteLine($"õlle järele läheb {kes} sest ta on noorim {oletus}");
            Console.WriteLine($"meie keskmine vanus on {summa / inimesed.Count}");

            Random r = new Random();   // seda teed üks kord

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(r.Next(100));  // seda võid teha mitu korda - saad juhuslikke arve

            }





        }
    }
}
