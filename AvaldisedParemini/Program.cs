﻿using System;

namespace AvaldisedParemini
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine(
            DateTime.Today.DayOfWeek switch
            {
                DayOfWeek.Sunday => "päevitame",
                DayOfWeek.Saturday => "saunatame",
                DayOfWeek.Monday => "parandame",
               // DayOfWeek.Tuesday => "läheme koju",
                _ => "ei tee midagi"  // any else case

            }
            );

        }
    }
}
