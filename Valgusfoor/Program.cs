﻿using System;

namespace Valgusfoor
{
    class Program
    {
        static void Main(string[] args)
        {
            

            Console.WriteLine("kas tahad iffiga (i) või switchiga (s)");
            char v = Console.ReadKey().KeyChar;
            if (v == 'i' || v == 'I')
            {
                Console.WriteLine("\rVaata valgusfoori (iffiga)");
                Console.Write("mis värvi tuli seal on: ");
                string värv = Console.ReadLine().ToLower();
                if (värv == "green" || värv == "roheline") Console.WriteLine("sõida edasi");
                else if (värv == "red" || värv == "punane") Console.WriteLine("jää seisma");
                else if (värv == "yeallow" || värv == "kollane") Console.WriteLine("oota rohelist");
                else Console.WriteLine("värvipime?");
            }
            else
            {
                Console.WriteLine("\rVaata valgusfoori (switchiga)");
                Console.Write("mis värvi tuli seal on: ");
                string värv = Console.ReadLine();
                switch (värv.ToLower())
                {
                    case "roheline":
                    case "green":
                        Console.WriteLine("sõida julgelt edasi");
                        break;
                    case "kollane":
                    case "yellow":
                        Console.WriteLine("oota rohelist");
                        break;
                    case "punane":
                    case "red":
                        Console.WriteLine("jää kohe seisma");
                        break;
                    default:
                        Console.WriteLine("värvipime vä?");
                        break;
                }

            }        }
    }
}
