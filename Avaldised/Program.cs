﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avaldised
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int a = 17;
            int b = 8;
            
            Console.WriteLine(
                    a % b // see on avaldis
                );

            double e = 40_000_000.1234_5678_9;

            a = 0xffff; // 65535
            Console.WriteLine(a);

            long l = 0x1f2c_2c2c_3d3d; // 34274580118845
            int i = 0b0101_1101_0000_1111; // 0x5d0f e 23823


            double d;
            double s = Math.PI / 2;
            d = Math.Sin(s);
            Console.WriteLine("nurga {0} siinune on {1}", s, d);

            short s1 = 7;
            long s2 = 8;
            Console.WriteLine( (s1 + s2).GetType() );

            DateTime dat = DateTime.Today;
            if (dat.Month > 5 && dat.Month < 9) Console.WriteLine("Meil on suvi");
            if (dat.DayOfWeek == DayOfWeek.Sunday || dat.DayOfWeek == DayOfWeek.Saturday)
                Console.WriteLine("täna tohib õlutit juua");

            int l1 = 3;
            int l2 = 5;
            Console.WriteLine("{0} & {1} on {2}", l1, l2, l1 & l2); // bitwise and
            Console.WriteLine("{0} | {1} on {2}", l1, l2, l1 | l2); // bitwise or
            Console.WriteLine("{0} ^ {1} on {2}", l1, l2, l1 ^ l2); // bitwise xor

            Console.WriteLine(l1 << 2); // 12 0011 << kaks kohta 1100 - paremalt tulevad nullid
            Console.WriteLine(l2 >> 1); // 2  0101 >> ühe koha   0010 - otsast kukuvad maha

            a = (l1 = l2);  // l2 muutujasse l1 ja ühtlasi muutujasse a

            a += 7;  // muutujale a lisa 7;      a = a + 7;
            // *= -= /= |= ||=              b1 ||= (a == b);   b1 = b || (a == b)

            // kolme tõugu tehted
            // unaarsed, binaarsed ja ternaarsed
            // binaarne:  pikkus * laius
            // unaarne:         tulu = (- kulu)     - miinus     ! eitus

            a = (-l1);

            //   bool-avaldis ? jah-avaldis : ei-avaldis
            Console.WriteLine(
                    dat.DayOfWeek == DayOfWeek.Tuesday ? "aeg on lõpetada" : "jätkame"
                
                );


            Console.WriteLine(
                dat.DayOfWeek == DayOfWeek.Sunday ? "päevitame" :
                dat.DayOfWeek == DayOfWeek.Saturday ? "saunatame" :
                dat.DayOfWeek == DayOfWeek.Monday ? "parandame pead" :
                dat.DayOfWeek == DayOfWeek.Friday ? "hakkame j...ma" :
                "läheme tööle"                
                );

            
              








        }
    }
}
