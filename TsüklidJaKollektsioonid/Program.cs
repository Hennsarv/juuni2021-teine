﻿using System;
using System.Collections.Generic;

namespace TsüklidJaKollektsioonid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tere Henn!");

            // me rääkisime kahest plokist 
            // if (bool) { ... } else { ... }
            // switch (avaldis) { case value: .... break; ...}

            // rääkimata jäi lihtne blokk
            // { ... }

            string nimi = "Henn Sarv";

            { // muutuja, mis on bloki sees on bloki sees ainult
                int arv = 7;
                Console.WriteLine(arv*arv);
                
            }

            {
                double arv = 7.0F;
                Console.WriteLine(arv / 11);
            }

            // järgmine plokk on tsükkel - for

            if (false)
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine($"Täna ilm on ilus {i}.korda");
                }


                int j = 0;
                for (; j++ < 100;)
                {
                    if (j % 3 == 0) continue;  // mine järgmisele ringile
                    Console.WriteLine($"imelik on korrata sama asja {j}. korda");

                    if (j > 15) break; // lõpetame selle tsükli ära

                }

                //for(; ;) // selline asi on lõpmatu tsükkel
                //{
                //    Console.WriteLine("seda kordame me lõpmatuseni");
                //}

                // veel kaks tsüklit
                // while(boolean) {  ... }

                j = 0;
                while (j++ < 100)
                {
                    if (j % 3 == 0) continue;  // mine järgmisele ringile
                    Console.WriteLine($"imelik on korrata sama asja {j}. korda");

                    if (j > 15) break; // lõpetame selle tsükli ära

                }
                string color = "must";
                while (color != "roheline")
                {
                    Console.Write("mis märv seal on");
                    color = Console.ReadLine();
                    switch (color)
                    {
                        case "roheline": Console.WriteLine("sõida"); break;
                        case "punane": Console.WriteLine("seisa"); break;
                        case "kollane": Console.WriteLine("oota"); break;
                        default: Console.WriteLine("pese silmad"); break;
                    }

                }

                Console.WriteLine("\nteeme sama asja ka do-tsükliga\n");

                do
                {
                    Console.Write("mis värv: ");
                    color = Console.ReadLine();
                    switch (color)
                    {
                        case "roheline": Console.WriteLine("sõida"); break;
                        case "punane": Console.WriteLine("seisa"); break;
                        case "kollane": Console.WriteLine("oota"); break;
                        default: Console.WriteLine("pese silmad"); break;
                    }

                }
                while (color != "roheline"); 
            }

            List<string> nimed = new List<string> { "Henn", "Ants", "Peeter" };

            for (int i = 0; i < nimed.Count; i++)
            {
                Console.WriteLine($"Tere {nimed[i]}!");
            }

            // foreach tsükkel
            foreach (string n in nimed)
            { 
                Console.WriteLine($"tere {n}!"); 
            }

            int[] arvud = { 1, 2, 3, 4, 5 };
            for (int i = 0; i < arvud.Length; i++) arvud[i]++;
            // loogelised sulud tohib ära jätta, kui
            // body koosneb ühest lausest ja on nii lühike, et mahub 1-le reale

            //foreach (int arv in arvud) arv++; selline asi ei toimi



        }
    }
}
