﻿using System;

namespace ProgrammiPlokid
{
    
    class Program
    {
        static void Main(string[] args)
        {
            string nimi;
            Console.Write("Kes sa oled: ");     // kirjutab konsoolile ILMA reavahetuseta
            nimi = Console.ReadLine();

            Console.Write("Hello {0}!\n", nimi);   // kirjutab konsoolile ja lisab reavahetuse
            int arv = -7;
            Console.WriteLine("Arv on {0:0000} ja tema ruut on {1:0000}", arv, arv * arv);
            Console.WriteLine($"Arv on {(arv < 0 ? -arv : arv):0000} ja tema ruut on {arv * arv:0000}");


            if (
                DateTime.Today.DayOfWeek == DayOfWeek.Wednesday ||
                DateTime.Today.DayOfWeek == DayOfWeek.Tuesday ||
                DateTime.Today.DayOfWeek == DayOfWeek.Saturday
                )
            {
                Console.WriteLine("kolmapäevased asjad");
            }
            else if (DateTime.Today.DayOfWeek == DayOfWeek.Friday)
            {
                Console.WriteLine("reedesed asjad");
            }
            else
            {
                Console.WriteLine("muud asjad");
            }

            // --- if lausel on väike suurem vend switch

            Console.Write("mis on\n\t1. tervitamine \n\t2. joomine\n\t3. koju minemine");
            string valik = Console.ReadLine();

            switch (valik)
            {
                // iga case lõpus peab olema kas break; goto; või return; continue;
                case "1":
                case "esiteks":
                    // tervitamise tegevused
                    goto case "2";
                    
                case "2":
                    // joomise tegevused
                    break;
                case "3":
                    // koju minemise tegevused
                    break;
                default:
                    // sa ei tea ise ka, mis tahad teha
                    break;
            }

            switch(DateTime.Today.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                    // laupäevased ja pühapäevased tegemised
                    break;
                case DayOfWeek.Wednesday :
                    // kolmapäevased tegemised, mille järel kohe igapäevased tegemised
                    goto default;
                case DayOfWeek.Friday :
                    // reedesed tegemised
                    break;
                default:
                    // igapäevased tegemised
                    break;
            }

            





        }
    }
}
