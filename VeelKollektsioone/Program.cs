﻿using System;
using System.Collections.Generic;

namespace VeelKollektsioone
{
    class Program
    {
        static void Main(string[] args)
        {
            // erinevad kollektsioonid
            // massiiv

            int[] arvud1 = new int[10];
            int[] arvud2 = new int[] { 1, 2, 3, 4, 5 };
            int[] arvud3 = { 1, 2, 3, 4, 7 };

            string[] nimed = { "Henn", "Ants", "Peeter" };

            List<string> listNimed = new List<string>(10) { "Henn", "Ants", "Peeter" };

            listNimed.Add("Joosep");

            int VARVASTE_ARV = 5;
            string[] varbad = new string[VARVASTE_ARV * 2];

            List<int> arvudList = new List<int>() {3, 7, 11, 13 };
            SortedSet<int> arvudSet = new SortedSet<int>() { 3, 7, 11, 13 };
            HashSet<int> arvudHash = new HashSet<int>() { 13, 7, 12, 8 };

            arvudList.Add(7); // lubab duplikaate
            arvudSet.Add(7);  // ei luba
            arvudHash.Add(7);

            Console.WriteLine("\n\nList\n");
            foreach (int a in arvudList) Console.Write($"{a}, ");
            Console.WriteLine("\n\nSet\n");
            foreach (int a in arvudSet) Console.Write($"{a}, ");
            Console.WriteLine("\n\nHash\n");
            foreach (int a in arvudHash) Console.Write($"{a}, ");

            if (arvudList.Contains(7)) Console.WriteLine("\nlistis on 7");

            // kaks veidrikku - Stack ja Queue

            Stack<int> arvudStack = new Stack<int>(10);
            arvudStack.Push(7);
            arvudStack.Push(8);
            arvudStack.Push(3);
            arvudStack.Push(1);
            Console.WriteLine("\n\nStack\n");
            foreach (int a in arvudStack) Console.Write($"{a}, ");

            Console.WriteLine();
            while (arvudStack.Count > 0) Console.WriteLine(arvudStack.Pop());

            Queue<int> arvudQueue = new Queue<int>();
            arvudQueue.Enqueue(2);
            arvudQueue.Enqueue(7);
            arvudQueue.Enqueue(9);
            arvudQueue.Enqueue(1);
            arvudQueue.Enqueue(3);

            Console.WriteLine("\n\nQueue\n");
            foreach (int a in arvudQueue) Console.Write($"{a}, ");

            Console.WriteLine();
            while (arvudQueue.Count > 0) Console.WriteLine(arvudQueue.Dequeue());

            SortedDictionary<string, int> vanused = new SortedDictionary<string, int>
            {
                {"Henn", 66 },
                {"Ants", 40 },
                {"Peeter", 28 }
            };

            vanused.Add("Joosepson", 80);

            Console.WriteLine($"Antsu vanus on {vanused["Ants"]}");

            //vanused.Add("Henn", 40); // selline annab vea

            vanused["Henn"] = 40; // kirjutatakse üle
            vanused["Jaak"] = 36; // lisatakse


            foreach(string n in vanused.Keys)
                Console.WriteLine($"kodaniku {n} vanus on {vanused[n]}");

            int mitu = 0;
            double kokku = 0;
            foreach (int a in vanused.Values) kokku += a;
            Console.WriteLine($"keskmine vanus on {kokku / vanused.Count}");

            // see on kommentaar mis nagu oleks üks lisatud rida

        }
    }
}


