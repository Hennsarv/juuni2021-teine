﻿using System;
class Program
{

    /* siit kuni tärn kladkriipsuni on kommnetaar */
    public static void Main() 
    {
        string nimi = "Henn"; // definitsioon

        Console.WriteLine("Tere {0}!", nimi);   // korraldus


        #region MyRegion
        // millest koosneb programm

        // lause
        // definitsioon - defineerib muutuja
        // avaldised - arvutab midagi ja muu hulgas võib ka tulemuse salvestada
        // korraldused - meetodite väljakutsed

        // plokid
        // lihtblokk
        // if-lause
        // tsüklid
        // switchid
        // ...  
        #endregion

        int arv = 0; // muutuja defineerimine
                     // mis tüüpi muutujaga on tegemist
                     // muutuja nimi
                     // = esialgne väärtus

        bool b = true;
        int pikk = int.MaxValue;
        Console.WriteLine(pikk);

        string minunimi = "Henn\tSarv\t66"; // \-tähistab erimärke
        Console.WriteLine(minunimi);

        // @ esimese jutumärgi ees tähndab, et erimärke ei kasutata
        string filename = @"C:\Users\sarvi\OneDrive\Vali IT Slaidid";

        DateTime täna = DateTime.Today;
        Console.WriteLine(täna);


    }




}

