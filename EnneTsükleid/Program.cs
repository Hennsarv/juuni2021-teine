﻿using System;
using System.Collections.Generic; 

namespace EnneTsükleid
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int arv = 77;                   // see on muutuja - int tüüpi

            int[] arvud = new int[10];      // see on muutuja - palju int tüüpi asju
                                            // massiiv - array - hetkel 10 arvu
            int[] teised = arvud;
            teised[4] = 44;


            #region plusplus
            //Console.WriteLine(++arv);       // eile ununes paar tehet rääkimata
            //                                // ++, --, profiks, suffiks 
            #endregion

            arvud[3] = 8;                   // omistame 4.le arvule väärtuse 8
            Console.WriteLine(arvud[4]);    // trükime 5. arvu välja
            // massiivi elemendid saavad kohe omale vaikimisi seda tüüpi null-väärtuse

            // massiivi algväärtustamine (kui vaja)
            // kolm varianti - samaväärsed
            int[] arvud2 = new int[5] { 1, 2, 3, 7, 20 };
            int[] arvud3 = new int[] { 1, 2, 3, 7, 20 };
            int[] arvud4 = { 1, 2, 3, 7, 20 };

            Console.WriteLine(arvud4.Length); // masssiivi käest saab 'küsida' tema pikkust

            DateTime[] sünnipäevad =
                {
                    new DateTime(1955,3,7),
                    new DateTime(1980,2,4),
                    new DateTime(1966,6,7)
                };

            string[] nimed = { "Henn", "Ants", "Peeter" };

            // ------------------ paar keerulisemat asja

            int[,] tabel = new int[10, 7]; // see on 2-mõõtmeline massiiv 10-rida, 7-veergu
            int[,] tabel2 = { { 1, 2, 3 }, { 4, 5, 6 } }; // 2 rida 3 veergu

            Console.WriteLine(tabel[1, 3]); // 2. rea 4. veerg'
            // sellist asja paljudes teistes keeltes (ntx Java) ei ole

            // massiivide massiiv - koosneb massiividest (Javas kutsutakse seda 2-mõõtmeliseks massiiviks)
            int[][] segu = new int[3][];
            segu[0] = new int[4]; // iga massiiv selles reas tuleb eraldi tekitada
            segu[1] = new int[7];
            segu[1][4] = 17;

            int[][] segu2 = { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6, 7 } };

            int[,,] kuubik = { { { 1, 2, 3 }, { 4, 5, 6 } }, { { 7, 8, 9 }, { 10, 11, 12 } } } ;
            // kolmemõõtmeline

            int[][][] jora; // massiivide massiivides koosnev massiiv - tõeliselt jora

            Console.WriteLine(kuubik.Length); 
            // see ütleb, kui palju 3-mõõtmelises üksikuid 'lahtreid' on - kokku 12

            Console.WriteLine(segu2.Length); // see ütleb kaks
            Console.WriteLine(segu2[0].Length); // see ütleb, et 3:  new int[] { 1, 2, 3 }

            Console.WriteLine(kuubik.Rank); 
            // see ütleb, palju on mõõtmeid (hetkel 3, lihtmassiivil 1)
            Console.WriteLine(kuubik.GetLength(0)); // mitu 0 kihti, 1 rida, 2 veergu 
            Console.WriteLine(kuubik.GetLength(1)); // mitu 0 kihti, 1 rida, 2 veergu 
            Console.WriteLine(kuubik.GetLength(2)); // mitu 0 kihti, 1 rida, 2 veergu 

            // Issandale meelepärane on piirduda lihtsate 1-mõõtmeliste massiividega
            // kõik need keerulisemad asjad jäta teistele

            // ------------------------------------ massiividega esialgu kõik
            // ------------------------------------ listid ja muu jora

            // selleks lisame üles uue rea - using System.Collection.Generic;
            // kollektsioonidega (massiivid on lihtsalt üks liik neid) maadleme edaspidigi

            List<int> rida =new List<int>(); // tegemist oleks kahh nagu massiviga
                                             // aga selle liikmete arv on muutuv

            rida.Add(77); // {77}
            rida.Add(88); // {77,88}
            Console.WriteLine(rida[0]); // trükitakse 77
            rida.Add(99); // {77,88,99}
            Console.WriteLine(rida[1]); // trükitakse 88
            rida.Remove(88); // {77,99}
            Console.WriteLine(rida[1]); // trükitakse 99

            List<int> rida2 = new List<int>(20); // tekitan tühja listi
            List<int> rida3 = new List<int>() { 1, 77, 2, 33 };  // tekitan listi, kus on ees 4 asja)
            List<int> rida4 = new List<int> { 1, 77, 2, 33 };  // tekitan listi, kus on ees 4 asja)
            List<int> rida5 = new List<int> { }; // tekitan tühja listi

            rida2.Add(77);
            for (int i = 0; i < 20; i++)
            {
                rida2.Add(i);
                Console.WriteLine($"rivis on {rida2.Count} ja sinna mahub {rida2.Capacity}");
            }
            Console.WriteLine(rida2[10]);

        }
    }
}
