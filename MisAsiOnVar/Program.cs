﻿using System;

namespace MisAsiOnVar
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tere var andmetüüp :)!");

            // väike kordamine andmetüüpidest
            // Hommikupalvus: andmetüüp on klass ja klass on andmetüüp

            // järgmised kaks on samaväärsed,
            // ühel juhul on klassi nimi (Int32) teisel juhul võtmesõna (int)
            Int32 arv1 = 77;
            int arv2 = 77;
            Console.WriteLine(arv1.GetType().FullName);
            Console.WriteLine(arv2.GetType().FullName);
            // klasside nimed on helerohelised ja suure algustähega
            // võtmesõnad on väikese algustähega ja tumesinised

            // terve hulk andmetüüpide/klasside nimesid langeb kokku (double, string)
            Double d1 = 77;
            double d2 = 77;
            //            Console.WriteLine(d1.GetType().FullName);
            //            Console.WriteLine(d2.GetType().FullName);

            // paljudel andmetüüpidel ei ole oma võtmesõna (harvadel on)

            DateTime kp = DateTime.Today;
            Console.WriteLine(kp.GetType().FullName);

            // var EI OLE andmetüüp vaid var on laisa mehe unistus
            // Vaata Aga Ridadest ise sa vana translaatorimees
            var x = new int[1];// = kp - new DateTime(1955,3,7); // translaator vaatab muutuja algväärtust ja otsustab, mis tüüpi muutuja on
            Console.WriteLine(x.GetType().FullName);

            // var on mugavusteenus, var on lihtsustusteenus ja var on võimalusteenus
            // kasutan var, kui ma ei viitsi kirjutada int või DateTime
            // kasutan var, kui on liiga keeruline kirjutada KeyValuePair<string,int>
            // kasutan var, kui ma ei teagi, mis tüüpi asjaga on tegemist
            // kasutan var, kui tüübil ei olegi nime (anonüümne või ajutine andmetüüp)

            Console.WriteLine("\nLoeme üles kõik meetodid, mis x muutujal olla võivad\n");
            // mul pole ainugi, mis asi on System.Reflection.MethodInfo
            //foreach (var m in x.GetType().GetMethods()) Console.WriteLine( m.Name );

            //Console.WriteLine("\nvaata nüüd mis tüüpi asjaga on tegu\n");
            var mina = new { Nimi = "Henn", Vanus = 66, Kinganumber = 45 };
            //Console.WriteLine(mina);


            int a;
            string b;
            (a, b) = (66, "Henn"); // tuple expression
            Console.WriteLine(b);

            (string nimi, int vanus) h = ("Henn", 66);  // ValueTuple
            Console.WriteLine(h.GetType().FullName);
            h.vanus++;

            





        }
    }
}
